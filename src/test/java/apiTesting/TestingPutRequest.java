package apiTesting;

import static org.hamcrest.Matchers.*;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class TestingPutRequest {
  @Test
  public void updatingDataUsingPut() {
	  JSONObject request = new JSONObject();
	  request.put("name", "magudees");
	  request.put("job", "tester");
	  
	  //given().body(request.toJSONString()).put("https://reqres.in/api/users/2").then().statusCode(200).log().all();
	  
	  given()
	  .body(request.toJSONString())
	  .put("https://reqres.in/api/users/2").
	  then()
	  .statusCode(200).body("updatedAt", greaterThanOrEqualTo("2023-01-24T10:07:57.285Z"));
  }
}
