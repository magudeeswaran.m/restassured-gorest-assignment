package apiTesting;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class TestingGetUsingGivenWhenApproach {
  @Test
  public void testStatusCode() {
	  baseURI="https://reqres.in";
	  given().get("/api/users?page=2").then().statusCode(200);
	  
  }
  @Test
  public void testParticularValue() {
	  baseURI="https://reqres.in";
	  given().get("/api/users?page=2").then().body("data[1].email", equalTo("lindsay.ferguson@reqres.in"));
	  
  }
  
  @Test
  public void printAllValues() {
	  baseURI="https://reqres.in";
	  given().get("/api/users?page=2").then().log().all();
	  
  }
}
