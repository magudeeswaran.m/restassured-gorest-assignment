package apiTesting;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class RestAssuredAssignment {
  @Test
  public void assertCheckingLastName() {
	  baseURI="https://reqres.in";
	  given().get("/api/users/2").then().body("data.last_name", equalTo("Weaver"));
  }
  
}
