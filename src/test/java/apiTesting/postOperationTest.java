package apiTesting;

import static io.restassured.RestAssured.baseURI;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class postOperationTest {
  @Test
  public void postOperation() {
	  JSONObject request = new JSONObject();
	  request.put("name", "magudees");
	  request.put("job", "tester");
	  
	  System.out.println(request);
	  
	  baseURI="https://reqres.in/api";
	  given().body(request.toJSONString()).when().post("/users").then().statusCode(201);
  }
}
