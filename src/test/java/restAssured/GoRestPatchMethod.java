package restAssured;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class GoRestPatchMethod {
  @Test
  public void patchOperation() {
	  
	  JSONObject request = new JSONObject();
	  request.put("name", "magudeeswaran");
	  request.put("email", "magudeeswaran@gmail.com");
	  request.put("gender", "male");
	  request.put("status", "active");
	  
	  System.out.println(request);
	  
	  baseURI="https://gorest.co.in/public/v2/";
	  given().body(request.toJSONString()).header("Authorization","Bearer f6172f0de8ad1b800c2a8e0ae5ca16fd46d59ef1d65b655139819acb630e2b53").header("Accept","application/json").header("Content-Type","application/json").when().log().all().patch("users/193103").then().log().all();
	  
  }
}
