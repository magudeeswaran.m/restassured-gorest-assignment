package restAssured;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

public class GoRestDeleteMethod {
  @Test
  public void deleteMethod() {
	  
	  baseURI="https://gorest.co.in/public/v2/";
	  given().header("Authorization","Bearer f6172f0de8ad1b800c2a8e0ae5ca16fd46d59ef1d65b655139819acb630e2b53").header("Accept","application/json").header("Content-Type","application/json").when().log().all().delete("users/193103").then().log().all();
	  
  }
}
